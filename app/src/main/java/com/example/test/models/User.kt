package com.example.test.models

import android.provider.ContactsContract

data class User (val name:String, val email: ContactsContract.CommonDataKinds.Email, val phone: ContactsContract.CommonDataKinds.Phone){

}