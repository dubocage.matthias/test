package com.example.test.viewPage


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import com.example.test.R
import kotlinx.android.synthetic.main.fragment_infos.*

class InfosFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_infos, container, false)
        val webV = v.findViewById(R.id.webview) as WebView
        webV.settings.javaScriptEnabled = true
        val map = HashMap<String,String>()
        map.put(key="X-AP-Key",value="uD4Muli8nO6nzkSlsNM3d1Pm")
        map.put(key = "X-AP-DeviceUID",value ="trial")
        map.put(key="Accept",value="text/html")
        webV.loadUrl("https://test-pgt-dev.apnl.ws/html",map)
        webV.webViewClient = WebViewClient()
        return v
    }


}

